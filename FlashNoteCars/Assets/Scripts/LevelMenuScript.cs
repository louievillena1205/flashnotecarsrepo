﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelMenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameScript.score = 0;
        GameScript.mistakes = 0;
        GameScript.GameOver = false;
        Level2GameScript.score = 0;
        Level2GameScript.mistakes = 0;
        Level2GameScript.GameOver = false;
        Level3GameScript.score = 0;
        Level3GameScript.mistakes = 0;
        Level3GameScript.GameOver = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
