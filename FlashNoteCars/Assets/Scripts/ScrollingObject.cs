﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour {

    private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();

        rb2d.velocity = new Vector2(-7.5f, 0);
	}
	
	// Update is called once per frame
	void Update () {
        // If the game is over, stop scrolling.
        if (GameScript.GameOver == true || Level2GameScript.GameOver == true || Level3GameScript.GameOver == true)
        {
            rb2d.velocity = Vector2.zero;
        }
    }
}
