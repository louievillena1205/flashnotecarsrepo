﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCarScript : MonoBehaviour {

    private BoxCollider2D MyCar;

    // Use this for initialization
    void Start()
    {
        MyCar = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (GameScript.MyCarMove == true || Level2GameScript.MyCarMove == true || Level3GameScript.MyCarMove == true)
        {
            RepositionObject();
        }
    }

    public void RepositionObject()
    {
        Vector2 groundOffSet = new Vector2(1.0f, 0);
        MyCar.transform.position = (Vector2)MyCar.transform.position + groundOffSet;
    }
}
