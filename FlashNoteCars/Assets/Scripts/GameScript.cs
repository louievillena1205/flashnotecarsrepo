﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameScript : MonoBehaviour
{
    public static bool GameOver = false;
    public static bool timeUp = false;
    public static bool gamePaused = false;
    public static bool EnemyCarMove = false;
    public static bool MyCarMove = false;
    public static string red = "redcar";
    string st = "ABCDEFG";
    char c;
    public float timeLimit;
    public static int score = 0;
    public static int mistakes = 0;

    public Text ResultText;
    public Text ScoreText;
    public Text MistakesText;
    public Image FlashCard;

    public Canvas PopUpCanvas;
    public Text FinalResultText;
    public Text FinalScoreText;

    // Use this for initialization
    void Start ()
    {
        timeUp = false;
        GameOver = false;
        PopUpCanvas.enabled = false;
        timeLimit = 6.0f;
        c = st[Random.Range(0, st.Length)];
        FlashCard.sprite = Resources.Load<Sprite>("C-position-notes/" + c + Random.Range(1, 2));
    }
	
	// Update is called once per frame
	void Update ()
    {
        MistakesText.text = "MISTAKES: " + mistakes.ToString();
        ScoreText.text = "SCORE: " + score;
        if (!GameOver)
        {
            if (timeLimit > 0)
            {
                if (!gamePaused)
                {
                    timeLimit -= Time.deltaTime;
                    ResultText.text = "TIME LEFT: " + Mathf.Round(timeLimit);
                }
            }
            else
            {
                if (timeUp == false)
                {
                    timeUp = true;
                    KeyPressed("H");
                }
            }
        }
    }

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void KeyPressed(string ans)
    {
        gamePaused = true;
        if (ans == c.ToString())
        {
            if (score < 1000)
            {
                score = score + 100;
                ResultText.text = "CORRECT";
                MyCarMove = true;
                StartCoroutine("ReloadScene");
            }
            else
            {
                GameOver = true;
                PopUpCanvas.enabled = true;
                FinalResultText.text = "YOU WIN!";
                FinalScoreText.text = "Your Score: " + score;
                PlayerPrefs.SetInt("Score", score);
            }
        }
        else
        {
            if (mistakes >= 4)
            {
                GameOver = true;
                PopUpCanvas.enabled = true;
                FinalResultText.text = "YOU LOSE!";
                FinalScoreText.text = "Your Score: " + score;
                PlayerPrefs.SetInt("Score", score);
            }
            else {
                AddMistakes();
                ResultText.text = "IT'S " + c.ToString();
                EnemyCarMove = true;
                StartCoroutine("ReloadScene");
            }
        }
    }

    public void TryAgain()
    {
        score = 0;
        mistakes = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AddMistakes()
    {
        mistakes = mistakes + 1;
    }

    IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(1.0f);

        MyCarMove = false;
        EnemyCarMove = false;
        timeLimit = 6.0f;
        c = st[Random.Range(0, st.Length)];
        int number = Random.Range(1, 2);
        FlashCard.sprite = Resources.Load<Sprite>("C-position-notes/" + c + number);
        gamePaused = false;
        timeUp = false;
    }
}
