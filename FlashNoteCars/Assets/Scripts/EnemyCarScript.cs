﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCarScript : MonoBehaviour {

    private BoxCollider2D EnemyCar;

    // Use this for initialization
    void Start()
    {
        EnemyCar = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (GameScript.EnemyCarMove == true || Level2GameScript.EnemyCarMove == true || Level3GameScript.EnemyCarMove == true)
        {
            RepositionObject();
        }
    }

    public void RepositionObject()
    {
        Vector2 groundOffSet = new Vector2(1.5f, 0);
        EnemyCar.transform.position = (Vector2)EnemyCar.transform.position + groundOffSet;
    }
}
